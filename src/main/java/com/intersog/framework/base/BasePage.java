package com.intersog.framework.base;

import org.openqa.selenium.support.PageFactory;

public class BasePage {
    public BasePage(){
        PageFactory.initElements(DriverFactory.getInstance().getDriver(), this);
    }
}
