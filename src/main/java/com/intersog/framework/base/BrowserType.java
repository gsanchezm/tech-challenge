package com.intersog.framework.base;

public enum BrowserType {
    CHROME,
    FIREFOX
}
