package com.intersog.framework.base;

import io.github.bonigarcia.wdm.WebDriverManager;;
import org.openqa.selenium.Platform;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

import static com.intersog.framework.config.Constants.ZALENIUM_GRID_PORT;

public class DriverFactory {
    private static DriverFactory instance = new DriverFactory();

    public static DriverFactory getInstance(){
        return instance;
    }

    ThreadLocal<RemoteWebDriver> driver = new ThreadLocal<RemoteWebDriver>();

    public RemoteWebDriver getDriver(){
        return driver.get();
    }

    public RemoteWebDriver setDriver(BrowserType browser) throws MalformedURLException {
        ChromeOptions chromeOptions = null;
        FirefoxOptions firefoxOptions = null;

        switch (browser){
            case CHROME:
                WebDriverManager.chromedriver().setup();
                chromeOptions = new ChromeOptions();
                chromeOptions.setCapability(CapabilityType.PLATFORM_NAME, Platform.ANY);
                driver.set(new RemoteWebDriver(new URL(ZALENIUM_GRID_PORT), chromeOptions));
                break;
            case FIREFOX:
                WebDriverManager.firefoxdriver().setup();
                firefoxOptions = new FirefoxOptions();
                firefoxOptions.setCapability(CapabilityType.PLATFORM_NAME, Platform.ANY);
                driver.set(new RemoteWebDriver(new URL(ZALENIUM_GRID_PORT), firefoxOptions));
                break;
        }

        driver.get().manage().window().maximize();
        return driver.get();
    }

    public void removeDriver(){
        driver.get().quit();
        driver.remove();
    }
}
