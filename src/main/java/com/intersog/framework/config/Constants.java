package com.intersog.framework.config;

public class Constants {
    public static final String RESULT_FOLDER = System.getProperty("user.dir") + "/results/";
    public static final String RESOURCE_FOLDER = System.getProperty("user.dir") + "/src/main/resources/";

    public static final String WORKBOOK_NAME = RESOURCE_FOLDER + "IntersogData.xlsx";
    public static final String WORKSHEET_NAME = "InvalidData";

    public static final String URL = "https://labs.pluseq.com/authpage";
    public static final String ZALENIUM_GRID_PORT = "http://157.245.226.9:4444/wd/hub";

    public static final String HTML_REPORT = RESULT_FOLDER + "iu_report.html";
    public static final String EXTENT_CONFIG = RESOURCE_FOLDER + "extent-config.xml";

    public static final String SCREENSHOT_FOLDER = RESULT_FOLDER + "screenshot/";
}
