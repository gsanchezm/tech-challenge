package com.intersog.framework.utils;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;

import static com.intersog.framework.config.Constants.WORKBOOK_NAME;
import static com.intersog.framework.config.Constants.WORKSHEET_NAME;

public class ExcelUtils {
    private static XSSFWorkbook excelWBook;
    private static XSSFSheet excelWSheet;
    private static Cell cell;

    public static Object[][] getTableArray(){
        String[][] tabArray = null;

        try{
            FileInputStream excelFile = new FileInputStream(WORKBOOK_NAME);
            excelWBook = new XSSFWorkbook(excelFile);
            excelWSheet = excelWBook.getSheet(WORKSHEET_NAME);
            int startCol, starRow, ci, cj;
            int totalRows = excelWSheet.getLastRowNum();
            int totalCol = excelWSheet.getRow(0).getLastCellNum();

            tabArray = new String[totalRows][totalCol];
            ci = 0;

            for (starRow = 1; starRow<=totalRows;starRow++,ci++){
                cj=0;
                for (startCol=0;startCol<totalCol;startCol++,cj++){
                    tabArray[ci][cj]= getCellDataDDT(starRow,startCol);
                }
            }
        }catch (Exception ex){
            System.err.println("Class ExcelUtils | Method getTableArray | Exception desc: " + ex.getMessage());
        }
        return (tabArray);
    }

    public static String getCellDataDDT(int RowNum, int ColNum){
        String data = "";

        try {
            cell = excelWSheet.getRow(RowNum).getCell(ColNum);
            if(cell.getCellType()== CellType.STRING){
                data = cell.getStringCellValue();
            }else if(cell.getCellType() == CellType.NUMERIC){
                data = String.valueOf(cell.getNumericCellValue());
            }
        }catch(Exception ex){
            System.err.println("Class ExcelUtils | Method getCellDataDDT | Exception desc: " + ex.getMessage());
        }
        return data;
    }
}
