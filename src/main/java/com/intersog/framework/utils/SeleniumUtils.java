package com.intersog.framework.utils;

import com.intersog.framework.base.DriverFactory;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static com.intersog.framework.config.Constants.SCREENSHOT_FOLDER;

public class SeleniumUtils {
    private static String date;
    private static String dateTime;
    private static String file;

    public static void highLight(WebElement element){
        WaitUtils.waitForElementVisible(element);
        for(int i = 0; i<3; i++){
            JavascriptExecutor js = (JavascriptExecutor) DriverFactory.getInstance().getDriver();
            for(int iCnt = 0; iCnt < 3 ; iCnt++){
                try{
                    js.executeScript("arguments[0].setAttribute('style','background:yellow')", element);
                    Thread.sleep(20);
                    js.executeScript("arguments[0].setAttribute('style','background:')", element);
                }catch (Exception ex){
                    System.err.println("Class SeleniumUtils | Method highLight | Exception desc: " + ex.getMessage());
                }
            }
        }
    }

    public static String takeSnapShot(String error){
        date = new SimpleDateFormat("yyyyMMddd_HH").format(Calendar.getInstance().getTime().toString());
        dateTime = new SimpleDateFormat("yyyyMMddd_HHmmss").format(Calendar.getInstance().getTime().toString());
        file = createFolder(SCREENSHOT_FOLDER + date) + "/" + error + dateTime + ".png";

        TakesScreenshot scrShot = ((TakesScreenshot)DriverFactory.getInstance().getDriver());
        try{
            File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
            File DestFile = new File(file);
            FileUtils.copyFile(SrcFile,DestFile);
        }   catch (Exception ex){
           System.err.println("Class SeleniumUtils | Method takeSnapShot | Exception desc: " + ex.getMessage());
        }
        return file;
    }

    private static String createFolder(String folderName){
        File dir= new File(folderName);

        if(!dir.exists()){
            System.out.println("Creating directory: " + dir.getName());
            boolean result = false;

            try {
                dir.mkdir();
                result = true;
            }catch (Exception ex){
                System.err.println("Class SeleniumUtils | Method createFolder | Exception desc: " + ex.getMessage());
            }
            if(result){
                System.out.println("Dir Created!");
            }
        }
        return dir.toString();
    }
}
