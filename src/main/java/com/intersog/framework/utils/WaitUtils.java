package com.intersog.framework.utils;

import com.intersog.framework.base.DriverFactory;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitUtils {
    private static WebDriverWait wait = new WebDriverWait(DriverFactory.getInstance().getDriver(), 30, 100);
    private static String pageLoadStatus;

    public static void sync(){
        do {
            JavascriptExecutor js = (JavascriptExecutor) DriverFactory.getInstance().getDriver();
            pageLoadStatus = (String)js.executeScript("return document.readyState");
        }while (!pageLoadStatus.equals("complete"));
    }

    public static void waitForElementVisible(final WebElement element){
        wait.until(ExpectedConditions.visibilityOf(element));
    }
}
