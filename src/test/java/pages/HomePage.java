package pages;

import com.intersog.framework.base.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.intersog.framework.utils.SeleniumUtils.highLight;

public class HomePage extends BasePage {

    @FindBy(xpath = "//div[@role='alert']")
    private WebElement lblSuccess;

    @FindBy(xpath = "//input[@type='button']")
    private WebElement btnGoBack;

    public String getSuccessText(){
        highLight(lblSuccess);
        return lblSuccess.getText();
    }

    public void clickGoBack(){
        highLight(btnGoBack);
        btnGoBack.click();
    }
}
