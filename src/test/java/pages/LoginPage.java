package pages;

import com.intersog.framework.base.BasePage;
import com.intersog.framework.base.DriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.intersog.framework.config.Constants.URL;
import static com.intersog.framework.utils.SeleniumUtils.highLight;
import static com.intersog.framework.utils.WaitUtils.sync;

public class LoginPage extends BasePage {
    @FindBy(name = "login")
    private WebElement txtUserName;

    @FindBy(name = "password")
    private WebElement txtPassword;

    @FindBy(className = "text-danger")
    private WebElement lblError;

    private void setUSerName(String user){
        highLight(txtUserName);
        txtUserName.clear();
        txtUserName.sendKeys(user);
    }

    public void setPassword(String password){
        highLight(txtPassword);
        txtPassword.clear();
        txtPassword.sendKeys(password);
        txtPassword.submit();
    }

    public String getLabelError(){
        highLight(lblError);
        return lblError.getText();
    }

    public LoginCommand LoginAs(String userName){
        return new LoginCommand(userName);
    }

    public void navigateToWebSite(){
        DriverFactory.getInstance().getDriver().navigate().to(URL);
        sync();
    }

    public class LoginCommand{
        private String userName;
        private String password;

        public LoginCommand(String userName){
            this.userName = userName;
        }

        public LoginCommand withPassword(String password){
            this.password = password;
            return this;
        }

        public void login(){
            setUSerName(userName);
            setPassword(password);
        }
    }
}
