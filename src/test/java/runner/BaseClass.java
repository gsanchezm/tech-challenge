package runner;

import com.intersog.framework.base.BrowserType;
import com.intersog.framework.base.DriverFactory;
import com.intersog.framework.utils.ReportUtils;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.ITestResult;
import org.testng.annotations.*;
import pages.HomePage;
import pages.LoginPage;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

import static com.intersog.framework.config.Constants.EXTENT_CONFIG;
import static com.intersog.framework.config.Constants.HTML_REPORT;
import static com.intersog.framework.utils.SeleniumUtils.takeSnapShot;

public class BaseClass {

    public ReportUtils report;
    public LoginPage login;
    public HomePage home;

    @AfterMethod
    public void getResult(ITestResult result) {
        switch (result.getStatus()) {
            case ITestResult.FAILURE:
                logTestFail(result);
                break;
            case ITestResult.SUCCESS:
                logTestPass(result);
                break;
            case ITestResult.SKIP:
                logTestSkip(result);
                break;
        }
    }

    @BeforeTest
    @Parameters("browser")
    public void setUpTest(String browser) throws MalformedURLException {
        report = new ReportUtils();
        report.setExtent(new ExtentReports(HTML_REPORT, true));
        report.getExtent().loadConfig(new File(EXTENT_CONFIG));
        DriverFactory.getInstance().setDriver(BrowserType.valueOf(browser));

        login= new LoginPage();
        home = new HomePage();
        login.navigateToWebSite();
    }

    @AfterTest
    public void tearDown() {
        DriverFactory.getInstance().removeDriver();
        report.getExtentTest().log(LogStatus.INFO, "Browser Closed!");

        report.getExtent().endTest(report.getExtentTest());

        report.getExtent().flush();
    }

    @AfterSuite
    public void openReport() throws IOException {
        Desktop.getDesktop().open(new File(HTML_REPORT));
    }

    public void startTestCase(String testName, String desc){
        report.setExtentTest(report.getExtent().startTest(testName,desc));
    }

    private void logTestFail(ITestResult result) {
        report.getExtentTest().log(LogStatus.FAIL, result.getName() + " Failed due below issue: " +
                report.getExtentTest().addScreenCapture(takeSnapShot("error_")));
    }

    private void logTestPass(ITestResult result) {
        report.getExtentTest().log(LogStatus.PASS, result.getName()+ " Passed!");
    }

    private void logTestSkip(ITestResult result) {
        report.getExtentTest().log(LogStatus.SKIP, result.getName()+ " Skipped!");
    }
}
