package runner;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

import static com.intersog.framework.utils.ExcelUtils.getTableArray;

public class TestRunner extends BaseClass{

    @Test(dataProvider = "IntersogData")
    public void introduceInvalidCredentials(String userEmail, String userPass, String error){
        startTestCase("Login With Invalid Credentials " + userEmail + " " + userPass, "Verify if application displays error messages using wrong data");
        login.LoginAs(userEmail).withPassword(userPass).login();
        Assert.assertEquals(login.getLabelError(), error);
    }

    @Test(dataProvider = "IntersogData")
    public void validCredentials(String userEmail, String userPass){
        startTestCase("Login With Valid Credentials " + userEmail + " " + userPass, "Verify if user is able to login");
        login.LoginAs(userEmail).withPassword(userPass).login();
        Assert.assertEquals("Login successful!", home.getSuccessText());
        home.clickGoBack();
    }

    @DataProvider(name = "IntersogData")
    public static Object[][] getDataFromDataProvider(Method testMethod){
        Object[][] testArray;
        if (testMethod.getName().equalsIgnoreCase("introduceInvalidCredentials")){
            testArray = getTableArray();
        }else {
            testArray = new Object[][]{
                    {"jkirk@ufp.com","enterprise"}
            };
        }
        return testArray;
    }
}
